//////////////
//// CSE 02 Welcome Class
///
public class WelcomeClass{
  
  public static void main(String args[]){
    ///prints StudentAddress to terminal window
    System.out.println("-----------");
    System.out.println("| WELCOME |");
    System.out.println("-----------");
    System.out.println("^  ^  ^  ^  ^  ^");
    System.out.println("/ \\/ \\/ \\/ \\/ \\/ \\");
    System.out.println("<-B--R--N--2--2--2->");
    System.out.println("\\ /\\ /\\ /\\ /\\ /\\ /");
    System.out.println("v  v  v  v  v  v");
                       
    
  }
  
}