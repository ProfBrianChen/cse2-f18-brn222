// Brian Nguyen CSE 02 HW9 CSE2Linear.java
/* Write a program called CSE2Linear.java that prompts the user to enter 15 ints for students final grades in CSE2.
Check that the user only enters ints, and print an error message if the user enters anything other than an int. 
Print a different error message for an int that is out of the range from 0-100, and finally a third error message 
if the int is not greater than or equal to the last int.  Print the final input array.  
Next, prompt the user to enter a grade to be searched for. Use binary search to find the entered grade. 
Indicate if the grade was found or not, and print out the number of iterations used. 
 */ 

import java.util.Scanner;
import java.util.Random;

public class CSE2Linear{
  
  public static boolean rangeChecker(int gradeValue){ //Method used to check if the int of the User is within the range of 0-100
    if(gradeValue< 0 || gradeValue>100){ //Checks if the user's input is less than 0 or greater than 100.
      return false; //Returning false causes an error message, telling the user to input a valid int within the range
    }
    return true; //Returning true passes the int value into the next method, the lastNumChecker
  }

   public static boolean lastNumChecker(int gradeValue,int i,int gradesFinal[]){ //Method used to check if the int of the User is greater than or equal to their last input
    if(i==0){ //This first statement takes into account that the first int the user inputs can be any value because there is no previous value to check against.
      return true; //Final check before the program prompts the user for a grade to be searched.
    }
     if(gradeValue<=gradesFinal[i - 1]){ //Checks if the user's input was less than the previous value.
       return false; //Returning false causes an error message, telling the user to input a valid int greater than or equal to previous value.
     }
    return true; //Returns true to prompt the user for a grade to be searched.
   }
  
    public static int binarySearch(int gradesFinal[],int searchGrade){ //Method that takes all of the user's inputs in an array and checks if their grade searched is within that array via binary search
    int min = 0; //Sets the minimum of the array as index 0
    int max = gradesFinal.length - 1; //Sets the max of the array as it's last index
    int middle; //Initializes the middle index
    while(max >= min){
      middle = (min+max) / 2; //Determines the middle index value
      if(gradesFinal[middle] > searchGrade){ //Determines if to begin looking to the left of the middle value for another search for the value inputted.
        max = middle - 1;
      }
      if(gradesFinal[middle] < searchGrade){ //Determines if to begin looking to the right of the middle value for another search for the value inputted.
        min = middle + 1;
      }
      else if(gradesFinal[middle] == searchGrade){ //If middle index value is found, return that value.
        return middle;
      }
    }
  return min; //If the grade inputted is not within the array return 0, the first value but this is not used.    
  }
  
  public static int shuffleArray(int randomArray[]){
    
    for(int l=0; l<5 ; l++){
    int target = (int) (5 * Math.random() );
    int temp  = randomArray [target] ;
    randomArray [target] = randomArray[l];
    return randomArray[l]= temp;
    }
    return 1;
  }
    
  public static void main(String [] args){
    int [] gradesFinal = new int [5];
    Scanner myScanner = new Scanner ( System.in );
    System.out.print ("Enter 15 ascending ints for final grades in CSE2: ");
    
    
      for(int i=0; i<5; i++){  //This for loop accounts for the index within the array
      boolean intCheck = myScanner.hasNextInt();
      while(!intCheck){ //This while loops checks if the user's input was actually an integer or not.
        System.out.println("Error, input was not an integer. Enter an integer.  ");
        String junk = myScanner.next(); //Clears input
        intCheck = myScanner.hasNextInt(); //Accepts next input
      }
      int gradeValue = myScanner.nextInt(); //Accepts input if input is integer.
      boolean rangeCheck = rangeChecker(gradeValue); //runs the input into the range check method
      boolean lastNumCheck = lastNumChecker(gradeValue,i,gradesFinal); //runs the input into the number check method
      
          if(rangeCheck == true){ //if this condition is true allow passage into next condition
            if(lastNumCheck == true){ //if this condition is true allow passage into the value being added to the array at given index
             gradesFinal [i] = gradeValue; //accepts the value into the array at given index
            }
            else{
              System.out.print ("Error, integer is not greater than or equal to last value entered. Enter a valid integer. "); //Error message for last number check
              i--; //Decrements the i counter by 1 so that the new value is put in that place
            }
          }
          else{
            System.out.println("Error, integer is out of range, 0-100, enter a valid integer. "); //Error message for out of range check
            i--;
          }
      }
    for(int j=0; j<5; j++){ //Prints out final array 
      System.out.print(gradesFinal [j] + " ");
    }
    
    System.out.println("Enter a grade to search for: "); //Prompts user to enter the grade to be searched
    int searchGrade = myScanner.nextInt(); //Accepts the user's input of grade

    int gradeFound = binarySearch(gradesFinal,searchGrade); //Runs the user's grade through the binary search method
    gradeFound = gradesFinal[gradeFound]; //Accepts the user's grade input into the array and then saved in the variable of grade found
    if(searchGrade == gradeFound){ //If value exists in the array
       System.out.print(gradeFound + " was found in the list. ");
    }
    else{ //If value doesn't exist
      System.out.print(searchGrade + " was not found in the list. ");
    }
    
    int randomArray []= new int [5];
    for(int i=0; i<5; i++){
      randomArray[i] = gradesFinal[i];
    }

    int shuffledArray [] = new int [5];
    for(int i=5; i<5; i++){
    shuffledArray [i] = shuffleArray(randomArray);
    System.out.print(shuffledArray [i] + " ");
    }
    
  }  
}
