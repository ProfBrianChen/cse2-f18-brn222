//Brian Nguyen CSE 02 Lab 07 10/25/2018
//// A program which uses methods to generate random basic sentences
//

import java.util.Random;

public class storyGen{
  
  public static String Adjective(int adjectiveWord){
    String adjective = "";
    switch (adjectiveWord){
      case 1:
        adjective = "amazing";
        break;
      case 2:
        adjective = "brilliant";
        break;
      case 3:
        adjective = "cautious";
        break;
      case 4:
        adjective = "mischievous";
        break;
      case 5:
        adjective = "fierce";
        break;
      case 6:
        adjective = "scary";
        break;
      case 7:
        adjective = "silly";
        break;
      case 8:
        adjective = "lazy";
        break;
      case 9:
        adjective = "jolly";
        break;
      default:
        adjective = "delightful";
        break;
    }
    return adjective;
  }
  
  public static String SubjectNoun(int subNounWord){
    String subjectNoun ="";
    switch (subNounWord){
      case 1:
        subjectNoun = "dog";
        break;
      case 2:
        subjectNoun = "cat";
        break;
      case 3:
        subjectNoun = "kid";
        break;
      case 4:
        subjectNoun = "frog";
        break;
      case 5:
        subjectNoun = "fox";
        break;
      case 6:
        subjectNoun = "hare";
        break;
      case 7:
        subjectNoun = "bear";
        break;
      case 8:
        subjectNoun = "monkey";
        break;
      case 9:
        subjectNoun = "owl";
        break;
      default:
        subjectNoun = "rock";
        break;
    }
    return subjectNoun;
  }
  
  public static String PastVerbs(int verbWord){
    String pastVerb ="";
    switch (verbWord){
      case 1:
        pastVerb = "passed";
        break;
      case 2:
        pastVerb = "hit";
        break;
      case 3:
        pastVerb = "swept";
        break;
      case 4:
        pastVerb = "kicked";
        break;
      case 5:
        pastVerb = "punched";
        break;
      case 6:
        pastVerb = "hugged";
        break;
      case 7:
        pastVerb = "loved";
        break;
      case 8:
        pastVerb = "fed";
        break;
      case 9:
        pastVerb = "beat";
        break;
      default:
        pastVerb = "threw";
        break;
    }
    return pastVerb;
  }
  
  public static String ObjectNoun(int objNounWord){
    String objectNoun ="";
    switch (objNounWord){
      case 1:
        objectNoun = "apple";
        break;
      case 2:
        objectNoun = "banana";
        break;
      case 3:
        objectNoun = "orange";
        break;
      case 4:
        objectNoun = "watermelon";
        break;
      case 5:
        objectNoun = "pear";
        break;
      case 6:
        objectNoun = "durian";
        break;
      case 7:
        objectNoun = "dragonfruit";
        break;
      case 8:
        objectNoun = "lychee";
        break;
      case 9:
        objectNoun = "peach";
        break;
      default:
        objectNoun = "strawberry";
        break;
    }
    return objectNoun;
  }
  
  public static void main(String [] args){
    Random randomGenerator = new Random ();
    int adjectiveWord = randomGenerator.nextInt (10);
    String Adjective = Adjective(adjectiveWord);
    
    int subNounWord = randomGenerator.nextInt (10);
    String SubjectNoun = SubjectNoun(subNounWord);
    
    int verbWord = randomGenerator.nextInt (10);
    String PastVerbs = PastVerbs(verbWord);
    
    int objNounWord = randomGenerator.nextInt (10);
    String ObjectNoun = ObjectNoun(objNounWord);
    System.out.print ( "The" + Adjective + SubjectNoun + PastVerbs + "the" + Adjective + ObjectNoun );
    
  }  
}