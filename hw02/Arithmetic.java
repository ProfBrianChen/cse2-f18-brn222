// Brian Nguyen 9/11/18 CSE 02 Hwk 02
//// A program that calculates the price of items bought including PA sales tax of 6%

public class Arithmetic {
  
  public static void main (String [] args) {
  //Number of pairs of pants
  int numPants = 3;
  //Cost per pair of pants
  double pantsPrice = 34.98;
  
  //Number of sweatshirts
  int numShirts = 2;  
  //Cost per shirt
  double shirtPrice = 24.99;
    
  //Number of belts
  int numBelts = 1;
  //cost per belt
  double beltCost = 33.99;
    
  //the tax rate
  double paSalesTax = 0.06; 
    
  double totalCostOfPants = numPants*pantsPrice ; //total cost of pants
  double totalCostOfShirts = numShirts*shirtPrice ; //total cost of shirts
  double totalCostOfBelts = numBelts*beltCost ; //total cost of belts
    
  double salesTaxOnPants = totalCostOfPants*paSalesTax ; //sales tax on pants
  double salesTaxOnShirts = totalCostOfShirts*paSalesTax ; //sales tax on shirts
  double salesTaxOnBelts = totalCostOfBelts*paSalesTax ; //sales tax on belts
    
  salesTaxOnPants = ((int)(salesTaxOnPants*100))/100.0;
  salesTaxOnShirts = ((int)(salesTaxOnShirts*100))/100.0;
  salesTaxOnBelts = ((int)(salesTaxOnBelts*100))/100.0;  
    
  double totalCostOfPurchasesPreTax = totalCostOfPants + totalCostOfShirts + totalCostOfBelts ; // Total cost of purchases (before tax)
  double totalSalesTax = salesTaxOnPants + salesTaxOnShirts + salesTaxOnBelts ; // Total sales tax
  double totalPaidForTransactionPostTax = totalCostOfPurchasesPreTax + totalSalesTax ; //Total price for transaction including sales tax
 
  totalPaidForTransactionPostTax = ((int)(totalPaidForTransactionPostTax*100))/100.0;
    
// All outputs to be displayed
System.out.println("Total Cost of Pants: $" + totalCostOfPants);
System.out.println("Total Cost of Shirts: $" + totalCostOfShirts);
System.out.println("Total Cost of Belts: $" + totalCostOfBelts);

System.out.println("Sales tax on Pants: $" + salesTaxOnPants);
System.out.println("Sales tax on Shirts: $" + salesTaxOnShirts);
System.out.println("Sales tax on Belts: $" + salesTaxOnBelts);
    
System.out.println("Total cost of purchase without tax: $" + totalCostOfPurchasesPreTax);
System.out.println("Total sales tax: $" + totalSalesTax);
System.out.println("Total cost of purchase including tax: $" + totalPaidForTransactionPostTax);
       
  } 
  
}
