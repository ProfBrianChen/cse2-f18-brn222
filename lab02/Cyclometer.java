// Brian Nguyen 9/06/2018 CSE 02 Lab 02
//// A cyclometer that measures time passed in seconds and # of rotations of the front wheel during time passed

public class Cyclometer {
  // main method required for every Java program
  public static void main (String [] args) {
    
    // The input data
  
  int secsTrip1=480; //Time taken in seconds for trip one is 480
  int secsTrip2=3220; //Time taken in seconds for trip two is 3220
  int countsTrip1=1561; //# of rotations during trip one is 1561
  int countsTrip2=9037; //# of rotations during trip two is 9037
  
// Intermediate variables and output data
  
  int WheelDiameter=27; // A variable assigned with value 27.0 representing the wheel's diameter
  double PI=3.14159; //Numerical value of Pi accurate to five digits after the variable
  double feetPerMile=5280; //The # of feet in a mile
  int inchesPerFoot=12; //The # if inches in a foot
  int secondsPerMinute=60; // The # of seconds in a minute
  double distanceTrip1, distanceTrip2,totalDistance; // Variable which takes the distances of trip one and trip two and the total distance traveled
  
  System.out.println("Trip 1 took "+
                     (secsTrip1/secondsPerMinute) +") minutes and had "+countsTrip1+" counts.");
  System.out.println("Trip 2 took "+
                    (secsTrip2/secondsPerMinute)+" minutes and had "+
                    countsTrip2+" counts.");
  
// Stored values and calculations
//// Calculating the # of rotations given distanced traveled
// Calculating the distance traveled for both trips
//// Calculating the total distance which is Trip1 plus Trip2
  
  distanceTrip1=countsTrip1*WheelDiameter*PI;
  
  // Above gives distance in inches
  //// For each count, a rotation of the wheel travels
  // the diameter in inches times PI
  
  distanceTrip1/=inchesPerFoot*feetPerMile; // Gives distance in miles
  distanceTrip2=countsTrip2*WheelDiameter*PI/inchesPerFoot/feetPerMile;
  totalDistance=distanceTrip1+distanceTrip2;
  
// Print out the output data
  System.out.println ("Trip 1 was "+distanceTrip1+" miles");
  System.out.println ("Trip 2 was "+distanceTrip2+" miles");
  System.out.println ("The total distance was "+totalDistance+" miles");
    
  }  //end of main method
  
} //end of class
  
