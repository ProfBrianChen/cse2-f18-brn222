// Brian Nguyen CSE 02 LAB 03
//// A program which uses the Scanner class to determine payment for dinner amongst friends
//

import java.util.Scanner;

public class Check{
  // main method required for every Java program
  
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner ( System.in ) ; // Creates an instance that will take input from STDIN:
      System.out.print ("Enter the original cost of the check in the form xx.xx: ") ; //User inputs the value of the check used to calculate final price per person
      double checkCost = myScanner.nextDouble () ; //accepts the User's input
    
      System.out.print("Enter the percentage tip that you wish to pay as a whole number (in the form xx) : ") ; //User inputs the value of percentage tip they would like to pay
      double tipPercent = myScanner.nextDouble () ; //accepts the User's input
      tipPercent /= 100 ; //Converts the percentage given into a decimal value
        
      System.out.print("Enter the number of people who went out to dinner: ") ; //User inputs the number of people to divide the final price by
      int numPeople = myScanner.nextInt () ; //accepts the User's input
    
    double totalCost; //identifies the totalCost
    double costPerPerson; //identifies costPerPerson
    int dollars,  //whole dollar amount of cost
        dimes, pennies; //for storing digits
                        //to the right of the decimal point
                        //for the cost$
    
    totalCost = checkCost * (1 + tipPercent) ;
    costPerPerson = totalCost / numPeople;
    //get the whole amount, dropping decimal fraction
    dollars =(int) costPerPerson;
    //get dimes amount, e.g.,
    // (int) (6.73 *10) % 10 -> 67 % 10 -> 7
    // where the % (mod) operator returns the remainder
    //after the division: 583%100 -> 83, 27%5 -> 2
    dimes =(int) (costPerPerson * 10) % 10; //essentially eliminates remainders for the tenths place
    pennies=(int) (costPerPerson * 100) % 10; //essentially eliminates remainders for the hundreths place
    System.out.println ("Each person in the group owes $" + dollars + '.' + dimes + pennies) ; //Final amount per person
    
  } //end of main method
} //end of class

