//Brian Nguyen CSE 02 HW 03
////A program that finds the volume of a pyramid given the base and height of a pyramid
//

import java.util.Scanner;

public class Pyramid{
  
  
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner ( System.in ) ;
      System.out.print ("Enter the length of one square side of the pyramid : ") ; // The length of one square side of the pyramid base
      double squareLength = myScanner.nextDouble () ; // Accepts user input of square length
    
      System.out.print ("Enter the height of the pyramid : ") ; // The height of the pyramid
      double heightLength = myScanner.nextDouble () ; // Accepts user input of height length
    
    double squareBase; // The area of the base of the pyramid
    double volumePyramid; // The volume of the pyramid 
    
      squareBase = squareLength * 4; // The equation to find the area of the base which is the squareLength times four
      volumePyramid = (squareBase * heightLength)/3 ; // The equation to find the volume of the pyramid which is the area of the base times the height divided by 3
    
    System.out.println ("The square side of the pyramid is : " + squareLength ) ; // Prints out the user's length of one square side of the base of the pyramid
    System.out.println ("The height of the pyramid is : " + heightLength ) ; // Prints out the user's height length of the pyramid
    System.out.println ("The volume inside the pyramid is: " + volumePyramid ) ; // Prints out the volume of the pyramid given all the inputs
    
  }
}