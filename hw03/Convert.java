//Brian Nguyen CSE 02 HW 03
////A program that converts numbers of acres of land affected and inches of rain to cubic miles
//

import java.util.Scanner;

public class Convert{
  
  
  public static void main(String[] args) {
    
    Scanner myScanner = new Scanner ( System.in ) ;
      System.out.print ("Enter the affected area in acres: ") ; // The affected area given in acres
      double affectedArea = myScanner.nextDouble () ; // Accepts the user's input
    
      System.out.print ("Enter the rainfall in the affected area: ") ; // The amount of rainfall within the given area
      double rainFall = myScanner.nextDouble () ; // Accepts the user's input 
    
    double acreInches; // The value of acres times amount of rainfall in acre inches
    double gallonsWater; // The value of acre inches in gallons of water
    double cubicMiles; // The value of gallons of water in cubic miles
    
      acreInches = affectedArea * rainFall; // Converts affected area in acres times rain fall in inches to acre inches
      gallonsWater = acreInches * 27154.285990761; // Converts the acre inches from above into gallons of water
      cubicMiles = gallonsWater * (9.0816858345662 * Math.pow (10,-13)); // Converts the gallons of water from above into cubic miles
    
    System.out.println ( cubicMiles + " cubic miles " ) ; // Prints out the full conversion from acre inces to cubic miles
        
  }
}