// Brian Nguyen CSE 02 Lab 04
//// A program that acts as card generator by picking numbers from 1-52
//

public class CardGenerator{
  
  public static void main (String[] args){
    
    int numberGenerated = (int)(Math.random() * 52+1 + 0);
    
    String suitName="";
    String cardIdentity="";
    
    if (numberGenerated <= 13){
     suitName = "Diamonds" ;
    }
    else if (numberGenerated <= 26){
     suitName = "Clubs" ;
    }
    else if (numberGenerated <= 39){
     suitName = "Hearts" ;
    }  
    else if (numberGenerated <= 52){
      suitName = "Spades" ;
    }
    
    switch (numberGenerated % 13){
      case 1:
        cardIdentity = "Aces" ;
        break;  
      case 11:
        cardIdentity = "Jack" ;
        break;
      case 12:
        cardIdentity = "Queen" ;
        break;
      case 0:
        cardIdentity = "King" ;
        break;
      default:
        cardIdentity = "" + numberGenerated % 13 ;
        break;
    }
    
    System.out.println ("You picked the " + cardIdentity + " of " + suitName );
    
    
  }
}