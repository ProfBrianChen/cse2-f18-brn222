//Brian Nguyen Lab 06 10/11/18
//// A program which creates patterns using loops, Part B
//

import java.util.Scanner;

public class PatternD{
  public static void main(String [] args){
  Scanner myScanner = new Scanner ( System.in );
  System.out.print ("Please input an integer between 1-10. ");
    
  int userInteger = myScanner.nextInt();  
  while (userInteger > 10){
  System.out.print ("Error! Number out of range! Please input an integer between 1-10. ");  
  userInteger = myScanner.nextInt();
  }
    
  for(int numRows = userInteger ; numRows >= 1; numRows--){
     for(int numNumbers = numRows ; numNumbers >= 1; numNumbers--){
       System.out.print(numNumbers + " ");
     }
  System.out.println();  
    }
  
  }
}