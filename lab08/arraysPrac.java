//Brian Nguyen CSE 02 Lab 08 11/08/18
////A program that randomly places integers between 0-99 into an array and then counts the number of occurences of each integer
//

public class arraysPrac{
  
  public static void main(String [] args){
    
    int[] randomNum; //Declares array randomNum which contains randomized integers from 0-99
    randomNum = new int [100]; //Allocates array meaning sets the size of the list for array randomNum
      
    int[] occurencesNum; //Declares array occurencesNum which contains the number of occurances of each number in the first array
    occurencesNum = new int [100]; //Allocates arrays, sets size of list for array occurencesNum
    
    for(int x = 0; x <= 99; x++ ){ // x represents the amount of times a random integer is generated, up to 99
      int rando = (int)(Math.random() * 100); //Generates random numbers for the first array between 0-99 until counter = 99
      randomNum [x] = rando; //stores the value of rando into the array a number of times until x reaches 99
      
      for(int i = 0; i <= 99; i++){ // a counter for the array occurencesNum
        if(i == randomNum [x]){ // declares that when int i is equal to a value in array randomNum [x]
          occurencesNum [i] += 1; //increment the value stored within that i value of occurencesNum by 1 
          
          //The for loop above represents the frequency counter, the amount of times a number from randomNum [x] appears
        }
      }
    }
    
    for(int i = 0; i <= 99; i++){ //This loop prints out the frequency of all the integers from 0-99
    System.out.println( i + " occurs " + occurencesNum[i] + " times");
    }
  }
}