//Brian Nguyen CSE 02 HW 04
//// A program that rolls dice or accepts given intergets to evaluate as slang terminology, this program uses switches rather than if else statements
//

import java.util.Scanner;

public class CrapsSwitch{
  
  
  public static void main(String[] args){
    
    Scanner myScanner = new Scanner ( System.in );
      System.out.print ("If you would like to cast dice, type \"1\" , if you would like to state two dice values type \"2\". ") ; //Asks the user if they would like to roll the dice or choose the dice values
      int answerChoice = myScanner.nextInt (); //Accepts the user's input
     
      String diceRoll=""; //Declares and initializes the diceRoll Variable
     
      int set1 = 0; //Declares and initializes the set1 Variable, also known as dice 1
      int set2 = 0; //Declares and initializes the set2 Variable, also known as dice 2
    
    switch (answerChoice){ //First switch to determine user's selected value
      case 1:   //If user choses to roll random dice, run random numbers
     set1 = (int)(Math.random() * 6+1 + 0) ; //Set one/Dice 1 random numbers
     set2 = (int)(Math.random() * 6+1 + 0) ; //Set one/Dice 2 random numbers
        
        //Below are all the cases for each dice number roll pair, meaning when dice one is a certain value and dice two is a certain value.
           switch (set1){
          case 1:
            switch (set2){
              case 1:
                diceRoll = "Snake Eyes" ;  
                  break;
            }
               break;
          case 2:
            switch (set2){
              case 1:
                diceRoll = "Ace Deuce" ;
                  break;      
            case 2:
                diceRoll = "Hard Four" ;
                  break;
            }
               break;
          case 3:
            switch (set2){
              case 1:
                diceRoll = "Easy Four" ;
                  break;
              case 2:
                diceRoll = "Fever five" ;
                  break;
              case 3:
                diceRoll = "Hard six" ;
                  break;
            }
               break;
          case 4:
            switch (set2){
              case 1:
                diceRoll = "Fever Five" ;
                  break;
              case 2:
                diceRoll = "Easy six" ;
                  break;
              case 3:
                diceRoll = "Seven out" ;
                  break;
              case 4:
                diceRoll = "Hard Eight" ;
                  break;
            }
               break;
          case 5:
            switch (set2){
              case 1:
                diceRoll = "Easy six" ;
                  break;
              case 2: 
                diceRoll = "Seven out" ;
                  break;
              case 3:
                diceRoll = "Easy Eight" ;
                  break;
              case 4:
                diceRoll = "Nine" ;
                  break;
              case 5:
                diceRoll = "Hard Ten" ;
                  break;
            }
               break;
          case 6:
            switch (set2){
              case 1:
                diceRoll = "Seven out" ;
                  break;
              case 2:
                diceRoll = "Easy Eight" ;
                  break;
              case 3:
                diceRoll = "Nine" ;
                  break;
              case 4:
                diceRoll = "Easy Ten" ;
                  break;
              case 5:
                diceRoll = "Yo-leven" ;
                  break;
              case 6:
                diceRoll = "Boxcars" ;
                  break;
            }
                  break;  
          }  
        
     break;
        
        
        
      case 2: // This is the case when the user wants to put in their own values for each dice, dice one's value cannot be lower than dice two
       System.out.print ("Value of dice 1? ") ; //Value of dice 1 inputted by the user
      set1 = myScanner.nextInt(); //Accepts the user's input
       System.out.print ("Value of dice 2? ") ; //Value of dice 2 inputted by the user
      set2 = myScanner.nextInt(); //Accepts the user's input  
  
     //Below are all the cases for each dice number roll pair, meaning when dice one is a certain value and dice two is a certain value.
        
      switch (set1){
          case 1:
            switch (set2){
              case 1:
                diceRoll = "Snake Eyes" ;  
                  break;
              default:
                System.out.println ("Dice 2 cannot be lower value than dice 1.");
                break;
            }
            break;
          case 2:
            switch (set2){
              case 1:
                diceRoll = "Ace Deuce" ;
                  break;      
            case 2:
                diceRoll = "Hard Four" ;
                  break;
            default:
              System.out.println ("Dice 2 cannot be lower value than dice 1.");
                break;
            }
            break;
          case 3:
            switch (set2){
              case 1:
                diceRoll = "Easy Four" ;
                  break;
              case 2:
                diceRoll = "Fever five" ;
                  break;
              case 3:
                diceRoll = "Hard six" ;
                  break;
              default:
                System.out.println ("Dice 2 cannot be lower value than dice 1.");
                  break;
            }
            break;
          case 4:
            switch (set2){
              case 1:
                diceRoll = "Fever Five" ;
                  break;
              case 2:
                diceRoll = "Easy six" ;
                  break;
              case 3:
                diceRoll = "Seven out" ;
                  break;
              case 4:
                diceRoll = "Hard Eight" ;
                  break;
              default:
                  System.out.println ("Dice 2 cannot be lower value than dice 1.");
                break;
            }
            break;
          case 5:
            switch (set2){
              case 1:
                diceRoll = "Easy six" ;
                  break;
              case 2: 
                diceRoll = "Seven out" ;
                  break;
              case 3:
                diceRoll = "Easy Eight" ;
                  break;
              case 4:
                diceRoll = "Nine" ;
                  break;
              case 5:
                diceRoll = "Hard Ten" ;
                  break;
              default:
                System.out.println ("Dice 2 cannot be lower value than dice 1.");
                  break;
            }
          break;
          case 6:
            switch (set2){
              case 1:
                diceRoll = "Seven out" ;
                  break;
              case 2:
                diceRoll = "Easy Eight" ;
                  break;
              case 3:
                diceRoll = "Nine" ;
                  break;
              case 4:
                diceRoll = "Easy Ten" ;
                  break;
              case 5:
                diceRoll = "Yo-leven" ;
                  break;
              case 6:
                diceRoll = "Boxcars" ;
                  break;
            }
                  break;  
          }
            
        }
    
    System.out.println ("You rolled " + diceRoll) ; //Prints out the user's dice pair as slang terminology
        
    }
    
  }
