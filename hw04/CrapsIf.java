//Brian Nguyen CSE 02 HW 04
//// A program that rolls dice or accepts given intergers to evaluate as slang terminology, this program uses only if and if else statements
//

import java.util.Scanner; 

public class CrapsIf{
  
  
   public static void main(String[] args){
     
     Scanner myScanner = new Scanner ( System.in );
       System.out.print ("If you would like to cast dice, type \"1\" , if you would like to state two dice values type \"2\". ") ; //Asks the user if they would like to roll the dice or choose the dice values
       int answerChoice = myScanner.nextInt (); //Accepts the user's input
     
       String diceRoll=""; //Declares and initializes the diceRoll Variable
     
      int set1 = 0; //Declares and initializes the set1 Variable, also known as dice 1
      int set2 = 0; //Declares and initializes the set2 Variable, also known as dice 2
     
     if (answerChoice == 1){ //If the user picks this answerChoice, set1 and set2 values are randomized and proceeds to the naming of the diceRoll
      set1 = (int)(Math.random() * 6+1 + 0) ;
      set2 = (int)(Math.random() * 6+1 + 0) ;
     }
     else if(answerChoice == 2){ //If the user picks this answerChoice, the user is asked to set the values for both dice within the range of 1-6
       System.out.print ("Value of dice 1? ") ; //Value of dice 1 inputted by the user
       set1 = myScanner.nextInt(); //Accepts the user's input
        if(set1 >= 7){ //If the user inputs a number greater than the range, this is the output which removes them from the program
          System.out.print ("Not a dice value! ");
          System.exit(0);
        }
       System.out.print ("Value of dice 2? ") ; //Value of dice 2 inputted by the user
       set2 = myScanner.nextInt(); //Accepts the user's input
        if(set2 >= 7){
          System.out.print ("Not a dice value! ");
          System.exit(0);
     }
   }
     //Below is the series of if and if else statements which contains the conditions for each value within the range of 1-6 for both set1 and set2
     ////Depending on the values of set1 and set2 and their relation to each other, the name of the pair is displayed
     
       if (set1 == set2){
         if(set1 == 1){
           diceRoll= "Snake Eyes" ;
         }
         else if(set1 == 2){
           diceRoll= "Hard four";
         }
         else if(set1 == 3){
           diceRoll = "Hard six";
         }
         else if(set1 == 4){
           diceRoll = "Hard Eight";
         }
         else if(set1 == 5){
           diceRoll = "Hard Ten";
         }
         else if(set1 == 6){
           diceRoll = "Boxcars";
         }
       }
       if(set2 == (set1 - 1 )){
         if(set2 == 1){
           diceRoll = "Ace Deuce";
         }
         else if(set2 == 2 ){
           diceRoll = "Fever Five";
         }
         else if(set2 == 3 ){
           diceRoll = "Seven Out";
         }
         else if(set2 == 4){
           diceRoll = "Nine";
         }
         else if(set2 == 5){
           diceRoll = "Yo-leven";
         }
       }
        if(set2 == (set1 - 2)){
          if(set2 == 1){
            diceRoll = "Easy Four";
          }
          else if(set2 == 2){
            diceRoll = "Easy six";
          }
          else if (set2 == 3){
            diceRoll = "Easy Eight";
          }
          else if(set2 == 4){
            diceRoll = "Easy Ten";
          }
        } 
        if(set2 == (set1 - 3)){
          if(set2 == 1){
            diceRoll = "Fever Five";
          }
          else if(set2 == 2){
            diceRoll = "Seven out";
          }
          else if(set2 == 3){
            diceRoll = "Nine";
          }
        }
        if(set2 == (set1 - 4)){
          if(set2 == 1){
            diceRoll = "Easy Six";
          }
          else if(set2 == 2){
            diceRoll = "Easy Eight";
          }
        }
       if(set2 == (set1 - 5)){
         if(set2 == 1){
           diceRoll = "Seven out";
         }  
       }
     
     if (set1 == set2){
         if(set1 == 1){
           diceRoll= "Snake Eyes" ;
         }
         else if(set1 == 2){
           diceRoll= "Hard four";
         }
         else if(set1 == 3){
           diceRoll = "Hard six";
         }
         else if(set1 == 4){
           diceRoll = "Hard Eight";
         }
         else if(set1 == 5){
           diceRoll = "Hard Ten";
         }
         else if(set1 == 6){
           diceRoll = "Boxcars";
         }
       }
       if(set1 == (set2 - 1 )){
         if(set1 == 1){
           diceRoll = "Ace Deuce";
         }
         else if(set1 == 2 ){
           diceRoll = "Fever Five";
         }
         else if(set1 == 3 ){
           diceRoll = "Seven Out";
         }
         else if(set1 == 4){
           diceRoll = "Nine";
         }
         else if(set1 == 5){
           diceRoll = "Yo-leven";
         }
       }
        if(set1 == (set2 - 2)){
          if(set1 == 1){
            diceRoll = "Easy Four";
          }
          else if(set1 == 2){
            diceRoll = "Easy six";
          }
          else if (set1 == 3){
            diceRoll = "Easy Eight";
          }
          else if(set1 == 4){
            diceRoll = "Easy Ten";
          }
        } 
        if(set1 == (set2 - 3)){
          if(set1 == 1){
            diceRoll = "Fever Five";
          }
          else if(set1 == 2){
            diceRoll = "Seven out";
          }
          else if(set1 == 3){
            diceRoll = "Nine";
          }
        }
        if(set1 == (set2 - 4)){
          if(set1 == 1){
            diceRoll = "Easy Six";
          }
          else if(set1 == 2){
            diceRoll = "Easy Eight";
          }
        }
       if(set1 == (set2 - 5)){
         if(set1 == 1){
           diceRoll = "Seven out";
         }  
       }
     
       System.out.println ("You rolled " + diceRoll); //Prints out the name of the pair that the user rolled or selected.
       
      }
     
    }