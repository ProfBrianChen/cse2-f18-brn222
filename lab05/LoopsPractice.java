// Brian Nguyen CSE 02 Lab 05
////A program which records a user's input of class information using loops
//

import java.util.Scanner;
public class LoopsPractice{  
public static void main(String[] args){
  Scanner myScanner = new Scanner ( System.in ); 
  System.out.print ("What is the course number? ");
  boolean intCourseNum = myScanner.hasNextInt();
  while (!intCourseNum){
    System.out.println("Not an integer type. Please input an integer. ");
    String junk = myScanner.next();
    intCourseNum = myScanner.hasNextInt();
  }
  int courseNumber = myScanner.nextInt();
/*
  System.out.print ("What is the department name? ");
  boolean stringActual = myScanner.hasNextDouble();
  while (!stringActual){
    System.out.println("Not a string type. Please input a string type. ");
    double junk2 = myScanner.nextDouble();
    double junk3 = myScanner.nextDouble();
    stringActual = myScanner.hasNextDouble();
  }
  double correctString = myScanner.nextDouble();
*/
  
  System.out.print ("How often does the class meet per week? ");
  boolean intClassMeet = myScanner.hasNextInt();
  while (!intClassMeet){
    System.out.println("Not an integer type. Please input an integer. ");
    String junk4 = myScanner.next();
    intClassMeet = myScanner.hasNextInt();
  }
  int meetingNumber = myScanner.nextInt();
  
  }
}
           