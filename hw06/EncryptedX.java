//Brian Nguyen HW 06 10/23/18
////A program which hides an X within astrieks
//
import java.util.Scanner; //Imports the scanner method

public class EncryptedX{ //declares the class name
  
  public static void main (String[] args){ //main method essential for every program
    
    Scanner myScanner = new Scanner ( System.in ); //declares scanner for use
    System.out.print ("Enter size of square. "); //Asks the user for the size of the square 
    boolean userInput = myScanner.hasNextInt ();
    while (!userInput){
      System.out.print("Please input an integer for the size of the square. ");
      String junk = myScanner.next();
      userInput = myScanner.hasNextInt ();
    }
    int squareSize = myScanner.nextInt ();
    
    for (int  w = 0; w < squareSize + 1  ; w++ ){
      for(int l = 0; l < squareSize + 1; l++){
        if( w == l ){
          System.out.print(" ");
        }
        else if ( l == (squareSize) - w){
          System.out.print (" ");
        }
        else{
          System.out.print("*");
        }
        
      }
      System.out.print ("\n");
    }  
  }
}  